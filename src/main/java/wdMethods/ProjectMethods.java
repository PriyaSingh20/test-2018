package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import week6.day2.ReadExcel;

public class ProjectMethods extends SeMethods{
	@BeforeSuite
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass
	public void beforeClass() {
		startTestCase();
	}

	@BeforeMethod
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);
	}
	@AfterMethod
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite
	public void afterSuite() {
		endResult();
	}
	
/*	@DataProvider(name = "Positive")
    public Object[][] fetchData()
    {
   	specifies 2 rows and 3 columns
   	 Object[][] data =  new Object[2][3];
   	 data[0][0] ="testleaf";company
   	 data[0][1] = "Priya";First Name
   	 data[0][2] = "P";Last Name
   	 
   	 data[1][0] = "test1";
   	 data[1][1] = "pp";
   	 data[1][2] = "S";
   	 return data;
   	 
    }*/
	
	@DataProvider(name = "fetchData")
	public Object[][] getdata() throws IOException
	{
		return ReadExcel.getExcelData(excelFileName);

	}
    }
	
	
	
	
	
	
	

