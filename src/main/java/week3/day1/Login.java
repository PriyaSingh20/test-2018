package week3.day1;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) 
	{
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		 ChromeDriver driver = new ChromeDriver();
         driver.manage().window().maximize();
         driver.get("http://leaftaps.com/opentaps");
         try {
			driver.findElementById("username").sendKeys("DemoSalesManager");
			// driver.findElementById("login").sendKeys("DemoSalesManager");
			 driver.findElementById("password").sendKeys("crmsfa");
			 driver.findElementByClassName("decorativeSubmit").click();
			 driver.findElementByLinkText("CRM/SFA").click();
driver.findElementByLinkText("Create Lead").click();
 driver.findElementById("createLeadForm_companyName").sendKeys("wipro");
driver.findElementById("createLeadForm_firstName").sendKeys("priya");
driver.findElementById("createLeadForm_lastName").sendKeys("test");
driver.findElementByClassName("smallSubmit").click();

//DrpDown//
//to select the dropdown//
WebElement src = driver.findElementById("createLeadForm_dataSourceId");
Select dropdown = new Select(src);
dropdown.selectByVisibleText("Direct Mail");

//to select the marketing campaign field//
WebElement src2 = driver.findElementById("createLeadForm_dataSourceId");
Select dropdown2 = new Select(src2);
List<WebElement> dropdown3 = dropdown2.getOptions();
int size = dropdown3.size();
dropdown2.selectByIndex(size-2);
		} 
         catch (NoSuchElementException e) 
         {
			System.out.println("Exception occured");// TODO Auto-generated catch block
			e.printStackTrace();
		}
  finally
  {
	  System.out.println("Exception Handled");
  }
         driver.close();
	}
	
}
