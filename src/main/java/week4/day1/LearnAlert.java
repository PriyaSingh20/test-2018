package week4.day1;


import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlert {

	public static void main(String[] args) throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		driver.switchTo().alert().sendKeys("priya P");
		driver.switchTo().alert().accept();
		
		/*Alert alert = driver.switchTo().alert();
		alert.sendKeys("check");
		alert.accept();
		Thread.sleep(5000);
		alert.accept();
		Thread.sleep(5000);
		//driver.close();
		*/
		
		

	}

}
