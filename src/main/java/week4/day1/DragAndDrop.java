package week4.day1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragAndDrop {

	public static void main(String[] args) 
	{
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://jqueryui.com/draggable");// TODO Auto-generated method stub
        Actions builder = new Actions(driver);
        driver.switchTo().frame(0);
        WebElement drag = driver.findElementById("draggable");
        int E = drag.getLocation().getX();
        int F = drag.getLocation().getY();
        builder.dragAndDropBy(drag, E+100, F+100);
        
	}

}
