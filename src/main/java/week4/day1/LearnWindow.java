package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindow {

	public static void main(String[] args) throws IOException 
	{
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://www.irctc.co.in/eticketing/userSignUp.jsf");
        driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
        driver.findElementByLinkText("Contact Us").click();
        //open the all window refernce without duplicates so we going to set and cannot directly use get method so we are converting to list/
        Set<String> allwindows = driver.getWindowHandles();
        List<String>listofwindows=new ArrayList<>();
        listofwindows.addAll(allwindows);
        //switching to second window//
        driver.switchTo().window(listofwindows.get(1));
        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
        //screenshot for second window//
        File src = driver.getScreenshotAs(OutputType.FILE);
        File dd = new File("./snaps/img.png");
        FileUtils.copyFile(src, dd);
        //moving to 1st window
        allwindows = driver.getWindowHandles();
        listofwindows=new ArrayList<>();
        listofwindows.addAll(allwindows);
        driver.switchTo().window(listofwindows.get(0));
        driver.close();
        
        
        
        
        
        
        
        
        
        
        // TODO Auto-generated method stub

	}

}
