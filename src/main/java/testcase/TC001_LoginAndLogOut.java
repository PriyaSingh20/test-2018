package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethodsOld.SeMethods;

public class TC001_LoginAndLogOut extends SeMethods{
	
	@Test
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement ele1 = locateElement("linktext","CRM/SFA");
		click(ele1);
		WebElement ele2 = locateElement("linktext","Create Lead");
		click(ele2);
		WebElement ele3 = locateElement("id","createLeadForm_companyName");
		type(ele3,"TestLeaf");
		WebElement ele4 = locateElement("id","createLeadForm_firstName");
		type(ele4,"Priya");
		WebElement ele5 = locateElement("id","createLeadForm_lastName");
		type(ele5,"P");
	
		WebElement ele6 = locateElement("class","smallSubmit");
		click(ele6);
		
		/*WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);*/
	}
	
}







