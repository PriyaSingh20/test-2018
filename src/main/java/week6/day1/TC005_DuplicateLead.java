package week6.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC005_DuplicateLead extends SeMethods
{
	@Test(groups= {"Smoke"})

	public void DuplicateLead() throws InterruptedException
	{
		startApp("chrome","http://leaftaps.com/opentaps/control/main");
		WebElement ele1 = locateElement("id","username");
		type(ele1,"DemoSalesManager");

		WebElement ele2 = locateElement("id","password");
		type(ele2,"crmsfa");
		
		WebElement ele3 = locateElement("class","decorativeSubmit");
		click(ele3);
		
		WebElement ele4 = locateElement("class","crmsfa");
		click(ele4);
		
}

	
}


