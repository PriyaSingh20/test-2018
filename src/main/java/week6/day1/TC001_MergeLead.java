package week6.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC001_MergeLead extends ProjectMethods
{
	
	@BeforeTest
	public void setdata()
	{
		testCaseName = "TC001_MergeLead";
		testCaseDesc = "Create a new lead";
		author = "priya";
		category = "smoke";
		
	}
	
	@Test(groups= {"Smoke"})
	public void MergeLead() throws InterruptedException
	{
		
		WebElement ele5 = locateElement("linktext","Leads");
		click(ele5);

		WebElement ele6 = locateElement("linktext","Merge Leads");
		click(ele6);

		/*WebElement ele7 = locateElement("id","//table[@id='widget_ComboBox_partyIdFrom']/table class/tbody/tr/td/table class[2])");
	click(ele7);*/

		/*WebElement ele8 = locateElement("xpath","//table[@class='dijit dijitReset dijitInline dijitLeft                                                                                dijitComboBoxNoArrow']/tbody/table/a[2]");
	click(ele8);*/

		WebElement ele7 = locateElement("xpath","//img[@src='/images/fieldlookup.gif'][1]");
		click(ele7);

		switchToWindow(1);

		WebElement ele8 = locateElement("LinkText","10334");
		type(ele8,"10334");
		

		WebElement ele9 = locateElement("xpath","//button[@class='x-btn-text']");
        click(ele9);
        

		WebElement ele10 = locateElement("LinkText","10334");
		clickWithNoSnap(ele10);

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		switchToWindow(0);

		Thread.sleep(3000);

		WebElement ele11 = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
        click(ele11);
        

		switchToWindow(1);

		WebElement ele12 = locateElement("Name","id");
        type(ele12,"10327");
        

		WebElement ele13 = locateElement("xpath","//button[@class='x-btn-text']");
        click(ele13);
        

		WebElement ele14 = locateElement("LinkText","10327");
        clickWithNoSnap(ele14);                             

		switchToWindow(0);

		WebElement ele15 = locateElement("LinkText","Merge");
        clickWithNoSnap(ele15);

		//Thread.sleep(5000);

		acceptAlert();
		WebElement ele16 = locateElement("LinkText","Find Leads");
        click(ele16);      


}
}







