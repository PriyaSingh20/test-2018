package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	/* declaring the static method as public*/
	public static Object[][] getExcelData(String fileName) throws IOException 
	{
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+ fileName+".xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
		
		int rowcount = sheet.getLastRowNum();
		System.out.println("Row count = "+rowcount);
		int columncount = sheet.getRow(0).getLastCellNum();
		System.out.println("Column count = "+columncount);
		
		/*creating the object*/
		Object[][] data = new Object[rowcount][columncount];
		for(int j = 1; j<=rowcount; j++)
		{
			XSSFRow row = sheet.getRow(j);
			for(int i=0;i<columncount; i++)
			{
				XSSFCell cell = row.getCell(i);
				data[j-1][i] = cell.getStringCellValue();
				
				/*String cellvalue = cell.getStringCellValue();*/                                                                                                                                    
			
			/*System.out.println(cellvalue);*/
		}
		}
		return data;
	}
}
