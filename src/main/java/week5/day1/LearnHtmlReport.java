package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnHtmlReport {

	public static void main(String[] args) throws IOException 
	{
		/*Readable Mode*/ /*Creating an object*/
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		/*Editable Mode*/
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		//TestCase level
		ExtentTest test = extent.createTest("TCOO1_CreateLead", "Create A new Lead");
	    test.assignCategory("Smoke");
	    test.assignAuthor("Priya");
	    
	    //testcase step level*/
	    test.pass("Browser launched Successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img10.png").build());
	    test.pass("The data DemoSalesManager entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
	    test.pass("The data crmsfa entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
	    test.fail("The data crmsfa not entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img16.png").build());
	    
	    extent.flush();

	

	}

}
