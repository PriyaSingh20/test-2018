package wdMethodsOld;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;


public class SeMethods implements WdMethods{
	public RemoteWebDriver driver;
	public int i = 1;
	public void startApp(String browser, String url) 
		{
		try
		{
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver  = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver  = new FirefoxDriver();
		}
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The Browser "+browser+" launched successfully");	
		}catch (WebDriverException e) 
		{
		System.err.println("The Browser "+browser+" not Launched");
		}
		finally
		{
		takeSnap();
	    }
		}

		public WebElement locateElement(String locator, String locValue) 
		{
		switch(locator) {
				case "id"	 : return driver.findElementById(locValue);
				case "class" : return driver.findElementByClassName(locValue);
				case "xpath" : return driver.findElementByXPath(locValue);
				case "linktext" : return driver.findElementByLinkText(locValue); 
				case "name" : return driver.findElementByName(locValue);
				}
		return null;
			}

		

		
	
	public WebElement locateElement(String locValue) {		
		return driver.findElementById(locValue);
	}

	public void type(WebElement ele, String data) 
	{
	try{
		ele.sendKeys(data);
		System.out.println("The Data "+data+" is entered Successfully");
	}catch (WebDriverException e) 
			{
		      System.out.println("The data "+data+" is Not Entered");
			}
	finally
	{
		takeSnap();
	}
	}
	public void clickWithNoSnap(WebElement ele) 
	{
		try {
			ele.click();
			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (Exception e) {
			System.err.println("The Element "+ele+"is not Clicked");
		}
	}

	public void click(WebElement ele) 
	{
		try
		{
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
		}catch(WebDriverException e)
		{
			System.err.println("The Element "+ele+"is not Clicked");
		} 
		finally {
				takeSnap();
			}
		
	}

	
	public String getText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value)
	{
		try
		{
		Select ee = new Select(ele);
		ee.selectByVisibleText(value);
		System.out.println("The DropDown Is Selected with VisibleText "+value);
		} catch (Exception e) {
			System.err.println("The DropDown Is not Selected with VisibleText "+value);
		} finally {
			takeSnap();
		}
		// TODO Auto-generated method stub

	}
	public void selectDropDownUsingvalue(WebElement ele, String value)
	{
		Select ee1 = new Select(ele);
		ee1.selectByValue(value);
		System.out.println("The DropDown Is Selected with value "+value);
	}
		
		
		
	

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) 
	{
		Select ee1 = new Select(ele);
		ee1.selectByIndex(i);
		System.out.println("The DropDown Is Selected with index "+index);

	}

	@Override
	public boolean verifyTitle(String expectedTitle) 
	{
		boolean breturn=false;
		String title = driver.getTitle();
		if(title.equals(expectedTitle))
		{
			breturn = true;
		}
		return breturn;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) 
	{
		Set<String> allwindows = driver.getWindowHandles();
		System.out.println("Total No. of Windows Opened : " +allwindows.size());
		List<String> listofwindows = new ArrayList<>();
		listofwindows.addAll(allwindows);
		driver.switchTo().window(listofwindows.get(index));
	}
	
		
		// TODO Auto-generated method stub

	

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void acceptAlert() 
	{
		driver.switchTo().alert().accept();// TODO Auto-generated method stub

	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		return null;
	}


	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File desc = new File("./snaps/img"+i+".png");		
		FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() 
	{
		driver.close();// TODO Auto-generated method stub

	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub

	}

}
